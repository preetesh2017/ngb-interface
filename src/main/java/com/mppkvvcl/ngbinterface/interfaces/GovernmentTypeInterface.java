package com.mppkvvcl.ngbinterface.interfaces;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface GovernmentTypeInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public String getType();

    public void setType(String type);
}

