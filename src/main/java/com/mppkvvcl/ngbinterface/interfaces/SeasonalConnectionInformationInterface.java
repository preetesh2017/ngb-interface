package com.mppkvvcl.ngbinterface.interfaces;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface SeasonalConnectionInformationInterface extends BeanInterface{
    public long getId();

    public void setId(long id);

    public String getConsumerNo();

    public void setConsumerNo(String consumerNo);

    public Date getSeasonStartDate();

    public void setSeasonStartDate(Date seasonStartDate);

    public String getSeasonStartBillMonth();

    public void setSeasonStartBillMonth(String seasonStartBillMonth);

    public Date getSeasonEndDate();

    public void setSeasonEndDate(Date seasonEndDate);

    public String getSeasonEndBillMonth();

    public void setSeasonEndBillMonth(String seasonEndBillMonth);

    public long getSeasonSubCategory();

    public void setSeasonSubCategory(long seasonSubCategory);

    public int getSeasonDuration();

    public void setSeasonDuration(int seasonDuration);

    public String getViolationMonth();

    public void setViolationMonth(String violationMonth);

    public BigDecimal getSeasonAverageConsumption();

    public void setSeasonAverageConsumption(BigDecimal seasonAverageConsumption);

    public BigDecimal getConsumptionViolationParameter();

    public void setConsumptionViolationParameter(BigDecimal consumptionViolationParameter);

    public BigDecimal getMaximumDemandViolationParameter();

    public void setMaximumDemandViolationParameter(BigDecimal maximumDemandViolationParameter);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getUpdatedOn();

    public void setUpdatedOn(Date updatedOn);

    public String getUpdatedBy();

    public void setUpdatedBy(String updatedBy);

}
