package com.mppkvvcl.ngbinterface.interfaces;

import java.util.Date;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface NSCStagingStatusInterface extends BeanInterface{
    public long getId();

    public void setId(long id);

    public long getNscStagingId();

    public void setNscStagingId(long nscStagingId);

    public String getStatus();

    public void setStatus(String status);

    public String getConsumerNo() ;

    public void setConsumerNo(String consumerNo);

    public String getRemark();

    public void setRemark(String remark);

    public String getCreatedBy() ;

    public void setCreatedBy(String createdBy) ;

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getUpdatedBy();

    public void setUpdatedBy(String updatedBy) ;

    public Date getUpdatedOn();

    public void setUpdatedOn(Date updatedOn);

    public NSCStagingInterface getNscStagingInterface();

    public void setNscStagingInterface(NSCStagingInterface nscStagingInterface);
}
