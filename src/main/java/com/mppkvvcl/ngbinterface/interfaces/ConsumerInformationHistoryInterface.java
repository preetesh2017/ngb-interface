package com.mppkvvcl.ngbinterface.interfaces;

import java.util.Date;

/**
 * Created by RUPALI on 9/22/2017.
 */
public interface ConsumerInformationHistoryInterface extends BeanInterface {

    public long getId();

    public void setId(long id);

    public String getPropertyName();

    public void setPropertyName(String propertyName);

    public String getPropertyValue();

    public void setPropertyValue(String propertyValue);

    public Date getEndDate();

    public void setEndDate(Date endDate);

    public String getEndBillMonth();

    public void setEndBillMonth(String endBillMonth);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public String getConsumerNo();

    public void setConsumerNo(String consumerNo);
}
