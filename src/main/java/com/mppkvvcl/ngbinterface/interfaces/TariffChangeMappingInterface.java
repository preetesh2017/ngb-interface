package com.mppkvvcl.ngbinterface.interfaces;

public interface TariffChangeMappingInterface extends BeanInterface {


    public long getId() ;

    public void setId(long id) ;

    public String getTariffCategory();

    public void setTariffCategory(String tariffCategory);

    public TariffDescriptionInterface getTariffDescription() ;

    public void setTariffDescription(TariffDescriptionInterface tariffDescription);
}
