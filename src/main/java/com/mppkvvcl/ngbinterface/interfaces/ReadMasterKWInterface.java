package com.mppkvvcl.ngbinterface.interfaces;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface ReadMasterKWInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public long getReadMasterId();

    public void setReadMasterId(long readMasterId);

    public BigDecimal getMeterMD();

    public void setMeterMD(BigDecimal meterMD);

    public BigDecimal getMultipliedMD();

    public void setMultipliedMD(BigDecimal multipliedMD);

    public BigDecimal getBillingDemand();

    public void setBillingDemand(BigDecimal billingDemand);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getUpdatedBy();

    public void setUpdatedBy(String updatedBy);

    public Date getUpdatedOn();

    public void setUpdatedOn(Date updatedOn);

}
