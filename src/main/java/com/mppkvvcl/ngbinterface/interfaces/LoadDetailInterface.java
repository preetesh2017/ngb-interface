package com.mppkvvcl.ngbinterface.interfaces;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface LoadDetailInterface extends BeanInterface{

    public static final long INITIAL_AGREEMENT_PERIOD_IN_DAYS = 730;

    public static final BigDecimal INITIAL_AGREEMENT_PERIOD_MULTIPLIER = new BigDecimal(0.5) ;

    public long getId();

    public void setId(long id);

    public String getConsumerNo();

    public void setConsumerNo(String consumerNo);

    public BigDecimal getSanctionedLoad();

    public void setSanctionedLoad(BigDecimal sanctionedLoad);

    public String getUnit();

    public void setUnit(String unit);

    public BigDecimal getContractDemand();

    public void setContractDemand(BigDecimal contractDemand);

    public String getCdUnit();

    public void setCdUnit(String cdUnit);

    public Date getEffectiveStartDate();

    public void setEffectiveStartDate(Date effectiveStartDate);

    public Date getEffectiveEndDate();

    public void setEffectiveEndDate(Date effectiveEndDate);

    public String getBillMonth();

    public void setBillMonth(String billMonth);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getUpdatedBy();

    public void setUpdatedBy(String updatedBy);

    public Date getUpdatedOn();

    public void setUpdatedOn(Date updatedOn);

    public String getPhase();

    public void setPhase(String phase);
}
