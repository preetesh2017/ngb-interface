package com.mppkvvcl.ngbinterface.interfaces;

/**
 * Created by RUPALI on 9/22/2017.
 */
public interface ConsumerGovernmentMappingInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public String getConsumerNo();

    public void setConsumerNo(String consumerNo);

    public String getGovernmentType();

    public void setGovernmentType(String governmentType);
}
