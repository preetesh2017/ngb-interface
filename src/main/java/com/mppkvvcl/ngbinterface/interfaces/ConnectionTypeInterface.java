package com.mppkvvcl.ngbinterface.interfaces;

/**
 * Created by RUPALI on 9/22/2017.
 */
public interface ConnectionTypeInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public String getType();

    public void setType(String type);

}
