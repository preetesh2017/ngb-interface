package com.mppkvvcl.ngbinterface.interfaces;

import java.util.Date;

/**
 * Created by PREETESH on 12/12/2017.
 */
public interface BillCalculationLineInterface extends BeanInterface {


    public long getId();

    public void setId(long id);

    public long getBillId();

    public void setBillId(long billId);

    public String getHead();

    public void setHead(String head);

    public String getUnit();

    public void setUnit(String unit) ;

    public String getRate();

    public void setRate(String rate);

    public String getAmount();

    public void setAmount(String amount);

    public String getStartSlab();

    public void setStartSlab(String startSlab);

    public String getEndSlab();

    public void setEndSlab(String endSlab);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

}
