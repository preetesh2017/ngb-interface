package com.mppkvvcl.ngbinterface.interfaces;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by PREETESH on 12/22/2017.
 */
public interface LoadFactorIncentiveConfigurationInterface extends BeanInterface {

    public static final int HOURS_IN_A_DAY = 24;

    public long getId();

    public void setId(long id);

    public BigDecimal getStartRange();

    public void setStartRange(BigDecimal startRange);

    public BigDecimal getEndRange();

    public void setEndRange(BigDecimal endRange);

    public String getRate();

    public void setRate(String rate);

    public Date getStartDate();

    public void setStartDate(Date startDate);

    public Date getEndDate();

    public void setEndDate(Date endDate);

    public BigDecimal getMultiplier();

    public void setMultiplier(BigDecimal multiplier);

}