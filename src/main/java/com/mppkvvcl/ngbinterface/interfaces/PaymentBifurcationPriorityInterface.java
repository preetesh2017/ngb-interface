package com.mppkvvcl.ngbinterface.interfaces;

/**
 * Created by PREETESH on 1/1/2018.
 */
public interface PaymentBifurcationPriorityInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public String getHead();

    public void setHead(String head);

    public int getPriority();

    public void setPriority(int priority);
}
