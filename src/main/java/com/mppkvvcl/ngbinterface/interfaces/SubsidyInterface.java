package com.mppkvvcl.ngbinterface.interfaces;

import java.math.BigDecimal;
import java.util.Date;

public interface SubsidyInterface extends BeanInterface {

    public static String CATEGORY_GENERAL = "GENERAL";
    public static String CATEGORY_SC = "SC";
    public static String CATEGORY_ST = "ST";

    public static boolean IS_BPL_TRUE = true;
    public static boolean IS_BPL_FALSE = false;

    public static String HEAD_ENERGY_CHARGE = "energy_charge";

    public static String HEAD_FIXED_CHARGE = "fixed_charge";

    public static String HEAD_FCA_CHARGE = "fca_charge";

    public long getId();

    public void setId(long id);

    public String getCategory();

    public void setCategory(String category);

    public boolean isBpl();

    public void setBpl(boolean bpl);

    public BigDecimal getSubsidy();

    public void setSubsidy(BigDecimal subsidy);

    public long getSlabStart();

    public void setSlabStart(int slabStart);

    public long getSlabEnd();

    public void setSlabEnd(int slabEnd);

    public long getSubcategoryCode();

    public void setSubcategoryCode(long subcategoryCode);

    public Date getEffectiveStartDate();

    public void setEffectiveStartDate(Date effectiveStartDate);

    public Date getEffectiveEndDate();

    public void setEffectiveEndDate(Date effectiveEndDate) ;

    public String getHead();

    public void setHead(String head);

    public BigDecimal getMultiplier();

    public void setMultiplier(BigDecimal multiplier);

    public boolean isSubcategoryViolation();

    public void setSubcategoryViolation(boolean subcategoryViolation);

}
