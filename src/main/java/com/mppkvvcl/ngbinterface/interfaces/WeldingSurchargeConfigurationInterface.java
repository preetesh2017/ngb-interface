package com.mppkvvcl.ngbinterface.interfaces;

import java.math.BigDecimal;
import java.util.Date;

public interface WeldingSurchargeConfigurationInterface extends BeanInterface{

    public long getId();

    public void setId(long id);

    public long getTariffId();

    public void setTariffId(long tariffId);

    public long getSubcategoryCode();

    public void setSubcategoryCode(long subcategoryCode);

    public String getRate();

    public void setRate(String rate);

    public BigDecimal getMultiplier();

    public void setMultiplier(BigDecimal multiplier);

    public BigDecimal getPowerFactorThreshold();

    public void setPowerFactorThreshold(BigDecimal powerFactorThreshold);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

}
