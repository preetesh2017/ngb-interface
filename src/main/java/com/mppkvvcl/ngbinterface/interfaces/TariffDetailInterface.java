package com.mppkvvcl.ngbinterface.interfaces;

import java.util.Date;
import java.util.List;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface TariffDetailInterface extends BeanInterface {

    public static final String  TARIFF_TYPE = "NORMAL";

    public long getId();

    public void setId(long id);

    public String getConsumerNo();

    public void setConsumerNo(String consumerNo);

    public String getTariffCode();

    public void setTariffCode(String tariffCode);

    public long getSubcategoryCode();

    public void setSubcategoryCode(long subcategoryCode);

    public Date getEffectiveStartDate();

    public void setEffectiveStartDate(Date effectiveStartDate);

    public Date getEffectiveEndDate();

    public void setEffectiveEndDate(Date effectiveEndDate);

    public String getBillMonth();

    public void setBillMonth(String billMonth);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getUpdatedBy();

    public void setUpdatedBy(String updatedBy);

    public Date getUpdatedOn();

    public void setUpdatedOn(Date updatedOn);

    public void setTariffInterfaces(List<TariffInterface> tariffInterfaces);

    public List<TariffInterface> getTariffInterfaces();
}
