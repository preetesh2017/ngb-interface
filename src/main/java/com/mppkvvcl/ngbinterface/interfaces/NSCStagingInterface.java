package com.mppkvvcl.ngbinterface.interfaces;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface NSCStagingInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public Date getConnectionDate();

    public void setConnectionDate(Date connectionDate);

    public String getConsumerName();

    public void setConsumerName(String consumerName);

    public String getConsumerNameH();

    public void setConsumerNameH(String consumerNameH);

    public String getRelativeName();

    public void setRelativeName(String relativeName);

    public String getRelation();

    public void setRelation(String relation);

    public boolean getIsBPL();

    public void setIsBPL(boolean isBPL);

    public String getBplNo();

    public void setBplNo(String bplNo);

    public String getCategory();

    public void setCategory(String category);

    public boolean getIsEmployee();

    public void setIsEmployee(boolean isEmployee);

    public String getEmployeeCompany();

    public void setEmployeeCompany(String employeeCompany);

    public String getEmployeeNo();

    public void setEmployeeNo(String employeeNo);

    public String getAddress1();

    public void setAddress1(String address1);

    public String getAddress2();

    public void setAddress2(String address2);

    public String getAddress3();

    public void setAddress3(String address3);

    public String getAddress1H();

    public void setAddress1H(String address1H);

    public String getAddress2H();

    public void setAddress2H(String address2H);

    public String getAddress3H();

    public void setAddress3H(String address3H);

    public String getPrimaryMobileNo();

    public void setPrimaryMobileNo(String primaryMobileNo);

    public String getAlternateMobileNo();

    public void setAlternateMobileNo(String alternateMobileNo);

    public String getAadhaarNo();

    public void setAadhaarNo(String aadhaarNo);

    public String getPan();

    public void setPan(String pan);

    public String getBankAccountNo();

    public void setBankAccountNo(String bankAccountNo);

    public String getBankAccountHolderName();

    public void setBankAccountHolderName(String bankAccountHolderName) ;

    public String getBankName();

    public void setBankName(String bankName);

    public String getIfsc();

    public void setIfsc(String ifsc);

    public String getEmailAddress();

    public void setEmailAddress(String emailAddress);

    public String getTariffCategory();

    public void setTariffCategory(String tariffCategory);

    public String getConnectionType();

    public void setConnectionType(String connectionType);

    public String getMeteringStatus();

    public void setMeteringStatus(String meteringStatus);

    public String getPremiseType();

    public void setPremiseType(String premiseType);

    public BigDecimal getSanctionedLoad();

    public void setSanctionedLoad(BigDecimal sanctionedLoad);

    public String getSanctionedLoadUnit();

    public void setSanctionedLoadUnit(String sanctionedLoadUnit);

    public BigDecimal getContractDemand();

    public void setContractDemand(BigDecimal contractDemand);

    public String getContractDemandUnit();

    public void setContractDemandUnit(String contractDemandUnit);

    public boolean getIsSeasonal();

    public void setIsSeasonal(boolean isSeasonal);

    public Date getSeasonStartDate();

    public void setSeasonStartDate(Date seasonStartDate);

    public Date getSeasonEndDate();

    public void setSeasonEndDate(Date seasonEndDate);

    public String getSeasonStartBillMonth();

    public void setSeasonStartBillMonth(String seasonStartBillMonth);

    public String getSeasonEndBillMonth();

    public void setSeasonEndBillMonth(String seasonEndBillMonth);

    public Long getPurposeOfInstallationId();

    public void setPurposeOfInstallationId(Long purposeOfInstallationId);

    public String getPurposeOfInstallation();

    public void setPurposeOfInstallation(String purposeOfInstallation);

    public String getTariffCode();

    public void setTariffCode(String tariffCode);

    public long getSubCategoryCode();

    public void setSubCategoryCode(long subCategoryCode);

    public String getPhase();

    public void setPhase(String phase);

    public Date getTcStartDate();

    public void setTcStartDate(Date tcStartDate);

    public Date getTcEndDate();

    public void setTcEndDate(Date tcEndDate);

    public boolean getIsGovernment();

    public void setIsGovernment(boolean isGovernment);

    public String getGovernmentType();

    public void setGovernmentType(String governmentType);

    public BigDecimal getPlotSize();

    public void setPlotSize(BigDecimal plotSize);

    public String getPlotSizeUnit();

    public void setPlotSizeUnit(String plotSizeUnit);

    public String getLocationCode();

    public void setLocationCode(String locationCode);

    public boolean getIsXray();

    public void setIsXray(boolean isXray);

    public BigDecimal getXrayLoad();

    public void setXrayLoad(BigDecimal xrayLoad);

    public long getNoOfDentalXrayMachine();

    public void setNoOfDentalXrayMachine(long noOfDentalXrayMachine);

    public long getNoOfSinglePhaseXrayMachine();

    public void setNoOfSinglePhaseXrayMachine(long noOfSinglePhaseXrayMachine);

    public long getNoOfThreePhaseXrayMachine();

    public void setNoOfThreePhaseXrayMachine(long noOfThreePhaseXrayMachine);

    public boolean getIsWeldingTransformerSurcharge();

    public void setIsWeldingTransformerSurcharge(boolean isWeldingTransformerSurcharge) ;

    public boolean getIsCapacitorSurcharge();

    public void setIsCapacitorSurcharge(boolean isCapacitorSurcharge);

    public boolean getIsDemandside();

    public void setIsDemandside(boolean isDemandside);

    public String getIsiMotorType();

    public void setIsiMotorType(String isiMotorType);

    public boolean getIsBeneficiary();

    public void setIsBeneficiary(boolean isBeneficiary);

    public String getDtrName();

    public void setDtrName(String dtrName);

    public String getPoleNo();

    public void setPoleNo(String poleNo);

    public String getPoleLatitude();

    public void setPoleLatitude(String poleLatitude);

    public String getPoleLongitude();

    public void setPoleLongitude(String poleLongitude);

    public String getFeederName();

    public void setFeederName(String feederName);

    public long getPoleDistance();

    public void setPoleDistance(long poleDistance);

    public String getAreaStatus();

    public void setAreaStatus(String areaStatus);

    public String getGroupNo();

    public void setGroupNo(String groupNo);

    public String getReadingDiaryNo();

    public void setReadingDiaryNo(String readingDiaryNo);

    public String getNeighbourConnectionNo();

    public void setNeighbourConnectionNo(String neighbourConnectionNo);

    public Date getSurveyDate();

    public void setSurveyDate(Date surveyDate);

    public String getMeterIdentifier();

    public void setMeterIdentifier(String meterIdentifier);

    public BigDecimal getStartRead();

    public void setStartRead(BigDecimal startRead);

    public boolean getHasCTR();

    public void setHasCTR(boolean hasCTR);

    public String getCtrIdentifier();

    public void setCtrIdentifier(String ctrIdentifier);

    public BigDecimal getCtrOverallMF();

    public void setCtrOverallMF(BigDecimal ctrOverallMF);

    public Date getMeterInstallationDate();

    public void setMeterInstallationDate(Date meterInstallationDate);

    public String getMeterInstallerName();

    public void setMeterInstallerName(String meterInstallerName);

    public String getMeterInstallerDesignation();

    public void setMeterInstallerDesignation(String meterInstallerDesignation);

    public boolean getHasModem();

    public void setHasModem(boolean hasModem);

    public String getModemNo();

    public void setModemNo(String modemNo);

    public String getSimNo();

    public void setSimNo(String simNo);

    public Date getDateOfRegistration();

    public void setDateOfRegistration(Date dateOfRegistration);

    public BigDecimal getRegistrationFeeAmount();

    public void setRegistrationFeeAmount(BigDecimal registrationFeeAmount);

    public String getRegistrationFeeAmountMRNo();

    public void setRegistrationFeeAmountMRNo(String registrationFeeAmountMRNo);

    public BigDecimal getSecurityDepositAmount();

    public void setSecurityDepositAmount(BigDecimal securityDepositAmount);

    public String getSecurityDepositAmountMRNo();

    public void setSecurityDepositAmountMRNo(String securityDepositAmountMRNo);

    public boolean getIsAffiliatedConsumer() ;

    public void setIsAffiliatedConsumer(boolean isAffiliatedConsumer);

    public String getAffiliatedConsumerNo() ;

    public void setAffiliatedConsumerNo(String affiliatedConsumerNo) ;

    public String getPortalName();

    public void setPortalName(String portalName);

    public String getPortalReferenceNo();

    public void setPortalReferenceNo(String portalReferenceNo);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getCreatedBy() ;

    public void setCreatedBy(String createdBy);

    public MeterMasterInterface getMeterMasterInterface();

    public void setMeterMasterInterface(MeterMasterInterface meterMasterInterface);

    public CTRMasterInterface getCtrMasterInterface();

    public void setCtrMasterInterface(CTRMasterInterface ctrMasterInterface);
}
