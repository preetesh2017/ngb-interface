package com.mppkvvcl.ngbinterface.interfaces;

import java.util.Date;

/**
 * Created by ANSHIKA on 22-09-2017.
 */
public interface TemporaryConnectionInformationInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public String getConsumerNo();

    public void setConsumerNo(String consumerNo);

    public Date getTcStartDate();

    public void setTcStartDate(Date tcStartDate);

    public Date getTcEndDate();

    public void setTcEndDate(Date tcEndDate);
}
