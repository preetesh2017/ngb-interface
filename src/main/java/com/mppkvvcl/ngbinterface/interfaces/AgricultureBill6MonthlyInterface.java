package com.mppkvvcl.ngbinterface.interfaces;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by RUPALI on 9/22/2017.
 */
public interface AgricultureBill6MonthlyInterface extends BeanInterface {
    public long getId();

    public void setId(long id);

    public String getConsumerNo();

    public void setConsumerNo(String consumerNo);

    public String getStartBillMonth();

    public void setStartBillMonth(String startBillMonth);

    public String getEndBillMonth();

    public void setEndBillMonth(String endBillMonth);

    public Date getIssueDate();

    public void setIssueDate(Date issueDate);

    public Date getDueDate();

    public void setDueDate(Date dueDate);

    public Date getChequeDueDate();

    public void setChequeDueDate(Date chequeDueDate);

    public BigDecimal getLoad();

    public void setLoad(BigDecimal load);

    public BigDecimal getUnit();

    public void setUnit(BigDecimal unit);

    public BigDecimal getEnergyCharge();

    public void setEnergyCharge(BigDecimal energyCharge);

    public BigDecimal getFixedCharge();

    public void setFixedCharge(BigDecimal fixedCharge);

    public BigDecimal getFca();

    public void setFca(BigDecimal fca);

    public BigDecimal getCapacitorSurcharge();

    public void setCapacitorSurcharge(BigDecimal capacitorSurcharge);

    public BigDecimal getActualBill();

    public void setActualBill(BigDecimal actualBill);

    public BigDecimal getSubsidy();

    public void setSubsidy(BigDecimal subsidy);

    public BigDecimal getCurrentBill();

    public void setCurrentBill(BigDecimal currentBill);

    public BigDecimal getSecurityDeposit() ;

    public void setSecurityDeposit(BigDecimal securityDeposit);

    public BigDecimal getSdInterest();

    public void setSdInterest(BigDecimal sdInterest);

    public BigDecimal getArrear();

    public void setArrear(BigDecimal arrear);

    public BigDecimal getOldArrearInstallment();

    public void setOldArrearInstallment(BigDecimal oldArrearInstallment);

    public BigDecimal getOldArrearInstallmentSubsidy();

    public void setOldArrearInstallmentSubsidy(BigDecimal oldArrearInstallmentSubsidy);

    public BigDecimal getSurcharge();

    public void setSurcharge(BigDecimal surcharge);

    public BigDecimal getCumulativeSurcharge();

    public void setCumulativeSurcharge(BigDecimal cumulativeSurcharge) ;

    public BigDecimal getNetBill();

    public void setNetBill(BigDecimal netBill);

    public BigDecimal getSurchargeArrear();

    public void setSurchargeArrear(BigDecimal surchargeArrear);

    public String getCreatedBy();

    public void setCreatedBy(String createdBy);

    public Date getCreatedOn();

    public void setCreatedOn(Date createdOn);

    public String getUpdatedBy();

    public void setUpdatedBy(String updatedBy);

    public Date getUpdatedOn();

    public void setUpdatedOn(Date updatedOn);


}
